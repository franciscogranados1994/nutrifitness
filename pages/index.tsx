import Head from 'next/head'
import Index from './Login/Index'
import style from './Styles/Main.module.scss'


const Home:React.FC = () => (
    <div className={style.main}>
    <Head>
      <title>First App</title>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;900&display=swap" rel="stylesheet"></link>
    </Head> 

    <main >
     <Index/>
    </main>

    <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>


  </div>
  
  
)


export default Home
