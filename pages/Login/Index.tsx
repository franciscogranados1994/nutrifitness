import React from "react";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import { useLogin } from "../hooks/useLogin";
import style from "../Styles/Login.module.scss";
import Background from './Background'

const Index: React.FC = () => {
  const {
    signInActive,
    signUpActive,
    activeSignIn,
    activeSignUp,
    activeReset,
    resetAccount,
  } = useLogin();
  return (
    <main className={style.main}>
      <section className={style.login_Bg}>
        <Background/>
      </section>
      <section className={style.login_section}>
        <div className={style.login_div}>
          <div className={style.login_div_center}>
            <div className={style.login_sign}>
              <h2 className={activeSignIn?style.login_h2_active: style.login_h2} onClick={() => signInActive()}>
                Sign In
              </h2>
              <h3 className={activeSignUp? style.login_h3_active: style.login_h3} onClick={() => signUpActive()}>
                Sign Up
              </h3>
            </div>
            <div className={style.login_reset}>
              <h2 className={activeReset? style.login_h2_active: style.login_h2} onClick={() => resetAccount()}>
                Reset
              </h2>
            </div> 
          </div>
        </div>
        <div className={style.login_div_sigin}>
          <SignIn
            activeSignIn={activeSignIn}
            activeSignUp={activeSignUp}
            activeReset={activeReset}
          />
        </div>
      </section>
    </main>
  );
};

export default Index;
