import React, { useState, useEffect } from "react";
import style from "../Styles/Background.module.scss";
import { stat } from "fs";

type optionsTypes = {
  active: Boolean;
};

const Background: React.FC = () => {
  const [options, setOptions] = useState<Boolean[]>([
    true,
    false,
    false,
    false,
  ]);

  const [indexOption, setIndexOption] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setIndexOption((indexOption) => indexOption + 1);
      if (indexOption >= 3) {
        setIndexOption(0);
      }

      const status = options.map((option, index) => {
        option = false;
        if (index === indexOption) {
          option = true;
        }
        return option;
      });

      setOptions(status);
    }, 3000);

    return () => clearInterval(interval);
  }, [indexOption]);


  return (
    <div className={style.div_main}>
      {options[0] ? (
        <div
          className={
            options[0]
              ? `${style.div_bg_first} ${style.div_bg_active}`
              : `${style.div_bg_first}`
          }
        >
          <h2 className={style.h2_bg_first}>Estimate </h2>
          <p className={style.p_bg_first}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo
            tempora fuga quibusdam facere autem est totam quo commodi unde,
            aspernatur praesentium mollitia aperiam molestiae. Cum corrupti
            vitae architecto tempore porro.
          </p>
        </div>
      ) : null}

      {options[1] ? (
        <div
          className={
            options[1]
              ? `${style.div_bg_second} ${style.div_bg_active}`
              : `${style.div_bg_second}`
          }
        >
          <h2 className={style.h2_bg_second}>Calculate </h2>
          <p className={style.h2_bg_second}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo
            tempora fuga quibusdam facere autem est totam quo commodi unde,
            aspernatur praesentium mollitia aperiam molestiae. Cum corrupti
            vitae architecto tempore porro.
          </p>
        </div>
      ) : null}

      {options[2] ? (
        <div
          className={
            options[2]
              ? `${style.div_bg_third} ${style.div_bg_active}`
              : `${style.div_bg_third}`
          }
        >
          <h2 className={style.h2_bg_third}>Find </h2>
          <p className={style.h2_bg_third}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo
            tempora fuga quibusdam facere autem est totam quo commodi unde,
            aspernatur praesentium mollitia aperiam molestiae. Cum corrupti
            vitae architecto tempore porro.
          </p>
        </div>
      ) : null}

      {options[3] ? (
        <div
          className={
            options[3]
              ? `${style.div_bg_fourth} ${style.div_bg_active}`
              : `${style.div_bg_fourth}`
          }
        >
          <h2 className={style.h2_bg_fourth}>Track </h2>
          <p className={style.h2_bg_fourth}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo
            tempora fuga quibusdam facere autem est totam quo commodi unde,
            aspernatur praesentium mollitia aperiam molestiae. Cum corrupti
            vitae architecto tempore porro.
          </p>
        </div>
      ) : null}

     
    </div>
  );
};

export default Background;
