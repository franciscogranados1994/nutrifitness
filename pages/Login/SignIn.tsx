import React, { useState, MouseEvent } from "react";
import sign from "../Styles/SignIn.module.scss";
import { useLogin } from "../hooks/useLogin";

/* Typing */
type userData = {
  email: string;
  password: string;
  fullname: string;
  username: string;
};

type signTypes = {
  activeSignIn: boolean;
  activeSignUp: boolean;
  activeReset: boolean;
};

/* Main component */
const SignIn: React.FC<signTypes> = ({
  activeSignIn,
  activeSignUp,
  activeReset,
}) => {
  const { signUpUser, signInUser, resetUserAccount } = useLogin();
  const [userData, setUserData] = useState<userData>({
    email: "",
    password: "",
    fullname: "",
    username: "",
  });

  async function handleFormData(event: React.ChangeEvent<HTMLInputElement>) {
   await setUserData({
      ...userData,
      [event.target.name]: event.target.value,
    });
  }

  return (
    <div className={sign.div_main}>
      <form className={sign.form}>
        <div
          className={
            activeSignIn
              ? `${sign.div_inputs}`
              : activeSignUp
                ? `${sign.div_inputs} ${sign.signup}`
                : `${sign.div_inputs} ${sign.reset}`
          }
        >
          <div className={sign.div_email}>
            <div
              className={
                activeSignIn
                  ? `${sign.div_transition}`
                  : activeSignUp
                    ? `${sign.div_transition} ${sign.signup}`
                    : `${sign.div_transition} ${sign.reset}`
              }
            ></div>
            <input
              className={sign.input_email}
              type="text"
              name="email"
              placeholder="Email"
              onChange={(e) => handleFormData(e)}
            />
          </div>
          <div className={sign.div_password}>
            <input
              className={sign.input_password}
              type="text"
              name="password"
              placeholder="Password"
              onChange={(e) => handleFormData(e)}
            />
          </div>
          <div className={sign.div_fullname}>
            <input
              className={sign.input_fullname}
              type="text"
              placeholder="Full Name"
              name="fullname"
              id="fullname"
              onChange={(e) => handleFormData(e)}
            />
          </div>
          <div className={sign.div_username}>
            <input
              className={sign.input_username}
              type="text"
              placeholder="Username"
              name="username"
              id="username"
              onChange={(e) => handleFormData(e)}
            />
          </div>
        </div>
        <div className={sign.div_handleData}>
          {activeSignIn ? (
            <button
              className={sign.button_signin}
              onClick={(event) => signUpUser(event, userData)}
            >
              Sign In
            </button>
          ) : null}
          {activeSignUp ? (
            <button
              className={sign.button_signup}
              onClick={(event) => signInUser(event, userData)}
            >
              Sign Up
            </button>
          ) : null}
          {activeReset ? (
            <button
              className={sign.button_reset}
              onClick={(event) => resetUserAccount(event, userData)}
            >
              Reset
            </button>
          ) : null}
        </div>
      </form>
    </div>
  );
};

export default SignIn;
