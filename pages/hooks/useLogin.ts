import React, { useState } from "react";


type formData = {
  email:string,
  password:string,
  fullname:string,
  username:string
}


export const useLogin = () => {
  const [activeSignIn, setSignIn] = useState<boolean>(true);
  const [activeSignUp, setSignUp] = useState<boolean>(false);
  const [activeReset, setReset] = useState<boolean>(false);

  async function signInActive() {
    await Promise.all([setReset(false), setSignIn(true), setSignUp(false)]);
  }

  async function signUpActive() {
    await Promise.all([setReset(false), setSignIn(false), setSignUp(true)]);
  }

  async function resetAccount() {
    await Promise.all([setReset(true), setSignIn(false), setSignUp(false)]);
  }

  /* Send the request */

  function signUpUser(event: React.MouseEvent<HTMLButtonElement>,userData: formData) {
    event.preventDefault();
    const {email,username, password, fullname} = userData
  }

  function signInUser(
    event: React.MouseEvent<HTMLButtonElement>,
    userData: formData
  ) {
    event.preventDefault();
    const {email, password} = userData;
  }

  function resetUserAccount(
    event: React.MouseEvent<HTMLButtonElement>,
    userData: formData
  ) {
    event.preventDefault();
    const {email} = userData;
  }

  return {
    activeSignIn,
    activeSignUp,
    signInActive,
    signUpActive,
    activeReset,
    resetAccount,
    signUpUser,
    signInUser,
    resetUserAccount,
  };
};
